package com.ubihinon.slideshow.viewController;

import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;

import com.ubihinon.slideshow.R;

/**
 * Created by ubihinon on 02.06.16.
 * This fragment shows the settings
 */
public class SettingsFragment extends PreferenceFragment
        implements Preference.OnPreferenceChangeListener {

    private ListPreference intervalList;
    private static final String AUTO_CHANGE_KEY     = "autoChange";
    private static final String CHANGE_INTERVAL_KEY = "changeInterval";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Make sure default values are applied.
         */
        PreferenceManager.setDefaultValues(getActivity(), R.xml.settings, false);
        /**
         * Load the preferences from an XML resource
         */
        addPreferencesFromResource(R.xml.settings);

        SwitchPreference autoChangeSwitch =
                (SwitchPreference) findPreference(AUTO_CHANGE_KEY);
        autoChangeSwitch.setOnPreferenceChangeListener(this);

        intervalList = (ListPreference) findPreference(CHANGE_INTERVAL_KEY);
        intervalList.setEnabled(autoChangeSwitch.isChecked());
    }


    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if(preference.getKey().equals(AUTO_CHANGE_KEY)) {
            intervalList.setEnabled((boolean)newValue);
        }
        return true;
    }
}
