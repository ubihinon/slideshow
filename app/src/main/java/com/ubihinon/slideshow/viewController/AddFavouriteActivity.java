package com.ubihinon.slideshow.viewController;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.ubihinon.slideshow.R;
import com.ubihinon.slideshow.helper.DbHelper;
import com.ubihinon.slideshow.model.Favourite;

/**
 * Created by ubihinon on 02.06.16.
 * This class gives opportunity to add comment in {@link Favourite#comment}
 */
public class AddFavouriteActivity extends AppCompatActivity {
    private Favourite favourite;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_favourite);

        final EditText commentEditText = (EditText) findViewById(R.id.commentEditText);
        final Button addCommentButton  = (Button) findViewById(R.id.addCommentButton);

        final Intent favouriteIntent = getIntent();
        favourite = favouriteIntent.getParcelableExtra("favourite");
        if (addCommentButton != null && commentEditText != null) {
            addCommentButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /**
                     * add comment in {@link Favourite#comment} and save in
                     * database
                     */
                    favourite.setComment(commentEditText.getText().toString());
                    DbHelper dbHelper = DbHelper.getInstance(getApplicationContext());
                    dbHelper.update(favourite);
                    /**
                     * return {@link Favourite} with comment to another activity
                     */
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("favourite", favourite);
                    setResult(Activity.RESULT_OK, resultIntent);

                    finish();
                }
            });
        }
    }
}
