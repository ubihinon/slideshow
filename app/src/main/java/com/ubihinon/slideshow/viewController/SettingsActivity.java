package com.ubihinon.slideshow.viewController;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ubihinon.slideshow.R;

import static com.ubihinon.slideshow.R.drawable.abc_ic_ab_back_mtrl_am_alpha;

/**
 * Created by ubihinon on 30.05.16.
 *
 */
public class SettingsActivity extends PreferenceActivity {
    public static final String AUTO_CHANGE          = "autoChange";
    public static final String SHOW_RANDOM_IMAGES   = "randomImages";
    public static final String CHANGE_INTERVAL      = "changeInterval";
    public static final String ANIMATION_TYPE       = "animationType";
    public static final String ONLY_FAVOURITES      = "onlyFavourites";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prepareLayout();
    }

    private void prepareLayout() {
        ViewGroup root = (ViewGroup) findViewById(android.R.id.content);
        View content = root.getChildAt(0);
        LinearLayout toolbarContainer = (LinearLayout)
                View.inflate(this, R.layout.activity_settings, null);

        root.removeAllViews();
        toolbarContainer.addView(content);
        root.addView(toolbarContainer);

        Toolbar toolbar = (Toolbar) toolbarContainer.findViewById(R.id.toolbar);
        toolbar.setTitle(getTitle());
        toolbar.setNavigationIcon(abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        /**
         * Put settings.xml to content_frame
         */
        getFragmentManager().beginTransaction()
                .replace(R.id.content_frame, new SettingsFragment()).commit();
    }
}
