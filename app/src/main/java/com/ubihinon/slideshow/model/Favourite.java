package com.ubihinon.slideshow.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ubihinon on 02.06.16.
 * This class keeps information about favourites
 */
public class Favourite implements Parcelable {
    private Integer idImage;
    private String comment;

    public Favourite() {
    }

    protected Favourite(Parcel in) {
        idImage = in.readInt();
        comment = in.readString();
    }

    public static final Creator<Favourite> CREATOR = new Creator<Favourite>() {
        @Override
        public Favourite createFromParcel(Parcel in) {
            return new Favourite(in);
        }

        @Override
        public Favourite[] newArray(int size) {
            return new Favourite[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idImage);
        dest.writeString(comment);
    }

    public Integer getIdImage() {
        return idImage;
    }

    public void setIdImage(Integer idImage) {
        this.idImage = idImage;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
