package com.ubihinon.slideshow;

import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.ubihinon.slideshow.helper.DbHelper;
import com.ubihinon.slideshow.helper.ExtendedArrayList;
import com.ubihinon.slideshow.helper.ImageLoader;
import com.ubihinon.slideshow.model.Favourite;
import com.ubihinon.slideshow.model.Image;
import com.ubihinon.slideshow.viewController.AddFavouriteActivity;
import com.ubihinon.slideshow.viewController.SettingsActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * This main application class which display images
 */
public class MainActivity extends AppCompatActivity implements LoaderManager
        .LoaderCallbacks<ArrayList<Image>> {
    private SharedPreferences preferences;
    private ImageSwitcher imageSwitcher;
    private TextView commentTextView;
    private ImageButton favouriteButton;
    private Handler favouriteHandler, timerHandler;
    private ArrayList<Image> images;
    private ExtendedArrayList favourites;
    private float lastX;
    private int position = 0;
    private final int IMAGE_LOADER = 1;
    private Timer timer = null;
    private DbHelper dbHelper;
    private ProgressDialog progressDialog;
    private TextView networkTextView;
    private IntentFilter networkStateChangedFilter;
    private BroadcastReceiver connectionChangeReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        networkTextView = null;
        images = new ArrayList<>();
        /**
         * Checks the internet connection
         */
        networkStateChangedFilter = new IntentFilter();
        networkStateChangedFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        connectionChangeReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive (final Context context, Intent intent) {
                internetConnectionStatus();
            }
        };
        internetConnectionStatus();

        dbHelper = DbHelper.getInstance(getApplicationContext());
        favourites = dbHelper.select();

        imageSwitcher   = (ImageSwitcher) findViewById(R.id.imageSwitcher);
        favouriteButton = (ImageButton) findViewById(R.id.favouriteButton);
        commentTextView = (TextView) findViewById(R.id.commentTextView);
        favouriteButton.setEnabled(false);

        imageSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                ImageView imageView = new ImageView(getApplicationContext());
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                LayoutParams params = new ImageSwitcher.LayoutParams(
                        LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT
                );
                imageView.setLayoutParams(params);
                return imageView;
            }
        });

        /**
         * Init loader for loading images
         */
        getLoaderManager().initLoader(IMAGE_LOADER, null, this);

        favouriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if(images == null || images.isEmpty()) {
                            return;
                        }
                        Message message = Message.obtain();
                        // not favourite
                        if(favourites.indexOfByImageId(images.get(position)) == -1) {
                            addToFavourite(message);
                        } else {
                            // favourite
                            deleteFromFavourite(message);
                        }
                    }
                }).start();

                favouriteHandler = new Handler(new Handler.Callback() {
                    @Override
                    public boolean handleMessage(Message msg) {
                        Toast.makeText(getApplicationContext(), (String) msg.obj,
                                Toast.LENGTH_SHORT).show();
                        setImageFavouriteButton();
                        return true;
                    }
                });
            }
        });
    }

    /**
     * Create/delete {@link #networkTextView} in order to display message that
     * network is unavailable
     * @param visible if it is true it create and show {@link #networkTextView}
     *                otherwise delete it.
     */
    private void createNetworkTextView(boolean visible) {
        if(visible) {
            if(networkTextView == null) {
                networkTextView = new TextView(getApplicationContext());
                networkTextView.setBackgroundResource(R.color.grey);
                networkTextView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
                        LayoutParams.MATCH_PARENT));
                networkTextView.setTextSize(20);
                networkTextView.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);

                networkTextView.setText(R.string.network_unavailable);
                RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
                if (mainLayout != null) {
                    mainLayout.addView(networkTextView);
                }
            }
            networkTextView.setVisibility(View.VISIBLE);
        } else {
            if(networkTextView != null) {
                networkTextView.setVisibility(View.INVISIBLE);
                networkTextView = null;
            }
        }
    }

    private void addToFavourite(Message message) {
        Favourite favourite = new Favourite();
        favourite.setIdImage(images.get(position).getId());
        favourite.setComment(null);
        favourites.add(favourite);
        dbHelper.insert(favourite);

        Intent favouriteActivity = new Intent(getApplicationContext(),
                AddFavouriteActivity.class);
        favouriteActivity.putExtra("favourite", favourite);
        startActivityForResult(favouriteActivity, 1);

        message.obj = getString(R.string.image_added_to_favourites);
        favouriteHandler.sendMessage(message);
    }

    private void deleteFromFavourite(Message message) {
        dbHelper.delete(
                favourites.get(
                        favourites.indexOfByImageId(images.get(position))
                )
        );
        favourites.remove(
                favourites.indexOfByImageId(images.get(position))
        );

        message.obj = getString(R.string.image_deleted_from_favourites);
        favouriteHandler.sendMessage(message);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == RESULT_OK && data != null) {
            Favourite favourite = data.getParcelableExtra("favourite");
            favourites.set(favourites.indexOfByImageId(images.get(position)),
                    favourite);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUI();
        try {
            registerReceiver(connectionChangeReceiver, networkStateChangedFilter);
            unregisterReceiver(connectionChangeReceiver);
            registerReceiver(connectionChangeReceiver, networkStateChangedFilter);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        internetConnectionStatus();
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            unregisterReceiver(connectionChangeReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id == R.id.action_settings) {
            // opens SettingsActivity
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    public void setPositionNext() {
        position++;
        if(position > images.size() - 1) {
            position = 0;
        }
    }

    public void setPositionPrevious() {
        position--;
        if (position < 0) {
            position = images.size() - 1;
        }
    }
    /**
     * Change images to next or previous on touch to left or right
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(images == null || images.isEmpty()) {
            return false;
        }
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                lastX = event.getX();
                break;
            }
            case MotionEvent.ACTION_UP: {
                float currentX = event.getX();
                // if left to right swipe on screen
                if (lastX < currentX) {
                    setPositionPrevious();
                    showImages(
                            preferences.getBoolean(
                                    SettingsActivity.ONLY_FAVOURITES, false
                            ),
                            false
                    );
                }
                // if right to left sweep event on screen
                if (lastX > currentX) {
                    setPositionNext();
                    showImages(
                            preferences.getBoolean(
                                    SettingsActivity.ONLY_FAVOURITES, false
                            ),
                            true
                    );
                }
                setImageFavouriteButton();
                break;
            }
        }
        return true;
    }

    private void updateUI() {
        if(images == null || images.isEmpty()) {
            return;
        }
        /**
         * Create/delete {@link timer}
         * depending on {@link SettingsActivity#AUTO_CHANGE}
         */
        if(preferences.getBoolean(SettingsActivity.AUTO_CHANGE, false)) {
            int changeInterval = Integer.parseInt(preferences.getString
                    (SettingsActivity.CHANGE_INTERVAL, "5000"));
            if(timer == null) {
                timer = new Timer();
                timer.schedule(new TimerImageTask(), 0, changeInterval);
            } else {
                timer.cancel();
                timer.purge();
                timer = null;
                timer = new Timer();
                timer.schedule(new TimerImageTask(), 0, changeInterval);
            }
        } else {
            if(timer != null) {
                timer.cancel();
                timer.purge();
                timer = null;
            }
        }
        /**
         * Calls via {@link timer} in order to put next image
         */
        timerHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                setPositionNext();
                showImages(
                        preferences.getBoolean(
                                SettingsActivity.ONLY_FAVOURITES, false
                        ),
                        true
                );
                setImageFavouriteButton();
                return true;
            }
        });
        /**
         * Show images random/by order depending on
         * {@link SettingsActivity#SHOW_RANDOM_IMAGES}
         */
        if(preferences.getBoolean(SettingsActivity.SHOW_RANDOM_IMAGES, false)) {
            long seed = System.nanoTime();
            Collections.shuffle(images, new Random(seed));
        } else {
            Collections.sort(images, new Comparator<Image>() {
                @Override
                public int compare(Image lhs, Image rhs) {
                    return lhs.getId().compareTo(rhs.getId());
                }
            });
        }
        /**
         * Sets animation type for {@link imageSwitcher}
         * depending on {@link SettingsActivity#ANIMATION_TYPE}
         */
        Animation inAnimation = null, outAnimation = null;
        int animationType = Integer.parseInt(
                preferences.getString(SettingsActivity.ANIMATION_TYPE, "0")
        );
        switch (animationType) {
            case 0:
                inAnimation = AnimationUtils.loadAnimation
                        (getApplicationContext(),R.anim.alpha_in);
                outAnimation = AnimationUtils.loadAnimation
                        (getApplicationContext(), R.anim.alpha_out);
                break;
            case 1:
                inAnimation = AnimationUtils.loadAnimation
                        (getApplicationContext(), R.anim.slide_down_in);
                outAnimation = AnimationUtils.loadAnimation
                        (getApplicationContext(), R.anim.slide_down_out);
                break;
            case 2:
                inAnimation = AnimationUtils.loadAnimation
                        (getApplicationContext(),R.anim.scale_in);
                outAnimation = AnimationUtils.loadAnimation
                        (getApplicationContext(),R.anim.scale_out);
                break;
        }
        imageSwitcher.setInAnimation(inAnimation);
        imageSwitcher.setOutAnimation(outAnimation);

        showImages(
                preferences.getBoolean(
                        SettingsActivity.ONLY_FAVOURITES, false
                ),
                true
        );

        setImageFavouriteButton();
    }

    @Override
    public Loader<ArrayList<Image>> onCreateLoader(int id, Bundle args) {
        ImageLoader imageLoader = null;
        if(id == IMAGE_LOADER) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setTitle(getString(R.string.download_images_progress_dialog));
            progressDialog.setMessage(getString(R.string.start_progress_dialog));
            progressDialog.show();
            imageLoader = new ImageLoader(getApplicationContext(), images,
                    getResources(), getPackageName(), this);
        }
        return imageLoader;
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<Image>> loader,
                               ArrayList<Image> data) {
        images = data;
        favouriteButton.setEnabled(true);
        if(progressDialog != null) {
            progressDialog.cancel();
        }
        getLoaderManager().destroyLoader(IMAGE_LOADER);
        internetConnectionStatus();
        updateUI();
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<Image>> loader) {
    }

    /**
     * Timer which change images
     * */
    private class TimerImageTask extends TimerTask {
        @Override
        public void run() {
            if(timerHandler != null) {
                timerHandler.sendEmptyMessage(0);
            }
        }
    }
    /**
     * Put drawable on {@link #favouriteButton}
     */
    private void setImageFavouriteButton() {
        if(images == null || images.isEmpty()) {
            favouriteButton.setVisibility(View.INVISIBLE);
            return;
        }
        favouriteButton.setVisibility(View.VISIBLE);

        // not favourite
        if(favourites.indexOfByImageId(images.get(position)) == -1
                || favourites.isEmpty()) {
            favouriteButton.setImageResource(R.drawable.favorites);
            fillCommentTextView(false, "");
        } else {
            // favourite
            favouriteButton.setImageResource(R.drawable.remove);

            if(images != null) {
                fillCommentTextView(true, favourites.get(favourites.
                        indexOfByImageId(images.get(position))).getComment());
            }
        }
    }
    /**
     * @param visible make {@link #commentTextView} visible/invisible
     * @param text text which will be displayed in {@link #commentTextView}
     */
    private void fillCommentTextView(boolean visible, String text) {
        commentTextView.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
        commentTextView.setText(text);
    }
    /**
     * @param onlyFavourites it shows only {@link Favourite} images if it is true
     *                       otherwise it shows all images
     * @param next it gets next image otherwise previous image
     */
    private void showImages(boolean onlyFavourites, boolean next) {
        if(images == null || images.isEmpty()) {
            return;
        }
        if(onlyFavourites && !favourites.isEmpty()) {
            // show only favourites
            if (favourites.indexOfByImageId(images.get(position)) != -1) {
                imageSwitcher.setImageDrawable(images.get(position).getDrawable());
            } else {
                while (favourites.indexOfByImageId(images.get(position)) == -1) {
                    if(next) {
                        setPositionNext();
                    } else {
                        setPositionPrevious();
                    }
                }
                imageSwitcher.setImageDrawable(images.get(position).getDrawable());
            }
        } else {
            // show all
            imageSwitcher.setImageDrawable(images.get(position).getDrawable());
        }
    }

    /**
     * Detect whether there is an Internet connection available
     * @return True if an Internet connection is available otherwise return false
     */
    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetInfo != null;
    }

    private void internetConnectionStatus() {
        if(isNetworkAvailable(getApplicationContext())
                && (images.isEmpty() || images == null)) {
            getLoaderManager().initLoader(IMAGE_LOADER, null, this);
        }
        createNetworkTextView(!isNetworkAvailable(getApplicationContext()));
    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }
}
