package com.ubihinon.slideshow.helper;

import com.ubihinon.slideshow.model.Favourite;
import com.ubihinon.slideshow.model.Image;

import java.util.ArrayList;

/**
 * Created by ubihinon on 02.06.16.
 *
 */
public class ExtendedArrayList extends ArrayList<Favourite> {

    public int indexOfByImageId(Image image) {
        for(int i = 0; i < this.size(); i++) {
            if(this.get(i).getIdImage().equals(image.getId())) {
                return i;
            }
        }
        return -1;
    }
}
