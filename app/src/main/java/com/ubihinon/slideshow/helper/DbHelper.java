package com.ubihinon.slideshow.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ubihinon.slideshow.model.Favourite;

/**
 * Created by ubihinon on 02.06.16.
 * This singleton works with database
 */
public class DbHelper extends SQLiteOpenHelper {
    private static DbHelper instance;
    private SQLiteDatabase db;
    public static final int DATABASE_VERSION = 3;
    public static final String DATABASE_NAME = "slide_show";
    public static final String FAVOURITE_TABLE_NAME = "favourite";
    public static final String COLUMN_ID_IMAGE = "id_image";
    public static final String COLUMN_COMMENT = "comment";

    private DbHelper(Context context, String name,
                     SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public static synchronized DbHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DbHelper(context.getApplicationContext(), DATABASE_NAME,
                    null, DATABASE_VERSION);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + FAVOURITE_TABLE_NAME + " (" +
                COLUMN_ID_IMAGE + " INTEGER UNIQUE NOT NULL, " +
                COLUMN_COMMENT + " VARCHAR(100));");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + FAVOURITE_TABLE_NAME);
        onCreate(db);
    }
    /**
     * Working with {@link Favourite}
     */
    public void insert(Favourite favourite) {
        db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_ID_IMAGE, favourite.getIdImage());
        contentValues.put(COLUMN_COMMENT, favourite.getComment());
        db.insert(FAVOURITE_TABLE_NAME, null, contentValues);
        db.close();
    }

    public void update(Favourite favourite) {
        db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_COMMENT, favourite.getComment());
        db.update(FAVOURITE_TABLE_NAME, contentValues, COLUMN_ID_IMAGE + " = ?",
                new String[]{String.valueOf(favourite.getIdImage())});
        db.close();
    }

    public void delete(Favourite favourite) {
        db = getWritableDatabase();
        db.delete(FAVOURITE_TABLE_NAME, COLUMN_ID_IMAGE + " = ?",
                new String[]{String.valueOf(favourite.getIdImage())});
        db.close();
    }

    public ExtendedArrayList select() {
        db = this.getWritableDatabase();
        Cursor cursor =
                db.query(FAVOURITE_TABLE_NAME, null, null, null, null, null, null);
        ExtendedArrayList favourites = new ExtendedArrayList();
        if (cursor.moveToFirst()) {
            while (cursor.moveToNext()) {
                Favourite favourite = new Favourite();
                favourite.setIdImage(cursor.getInt(0));
                favourite.setComment(cursor.getString(1));
                favourites.add(favourite);
            }
        }
        cursor.close();
        db.close();
        return favourites;
    }
}
