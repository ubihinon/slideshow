package com.ubihinon.slideshow.helper;

import android.content.Context;
import android.content.Loader;
import android.content.res.Resources;
import android.os.AsyncTask;

import com.ubihinon.slideshow.MainActivity;
import com.ubihinon.slideshow.model.Image;

import java.util.ArrayList;

/**
 * Created by ubihinon on 30.05.16.
 * This class download images
 */
public class ImageLoader extends Loader<ArrayList<Image>> {
    private ParseImages parseImages;
    private ArrayList<Image> images;
    private Resources resources;
    private String packageName;
    public MainActivity mainActivity;

    public ImageLoader(Context context, ArrayList<Image> images, Resources resources,
                       String packageName, MainActivity mainActivity) {
        super(context);
        this.images         = images;
        this.resources      = resources;
        this.packageName    = packageName;
        this.mainActivity   = mainActivity;
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
    }

    @Override
    protected void onForceLoad() {
        super.onForceLoad();
    }

    @Override
    protected void onAbandon() {
        super.onAbandon();
    }

    @Override
    protected void onReset() {
        super.onReset();
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if(!images.isEmpty()) {
            images.clear();
        }
        if(parseImages != null) {
            parseImages.cancel(true);
        }
        parseImages = new ParseImages();
        parseImages.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    class ParseImages extends AsyncTask<Void, String, ArrayList<Image>> {

        @Override
        protected ArrayList<Image> doInBackground(Void... params) {
            ImageParser parser = new ImageParser(images);
            String json = parser.loadJSONFromFile("data", resources, packageName);
            parser.parseImages(json);
            for (int i = 0; i < images.size(); i++) {
                try {
                    parser.setDrawablesFromUrl(i);
                    publishProgress(String.valueOf(i + 1));
                } catch (Exception e) {
                    images.remove(i);
                    i--;
                    e.printStackTrace();
                }
            }
            images = parser.getImages();
            return images;
        }

        @Override
        protected void onProgressUpdate (String...values){
            super.onProgressUpdate(values);
            mainActivity.getProgressDialog().setMessage(values[0] + "/" + images.size());
        }

        @Override
        protected void onPostExecute (ArrayList < Image > images) {
            super.onPostExecute(images);
            deliverResult(images);
        }
    }
}
