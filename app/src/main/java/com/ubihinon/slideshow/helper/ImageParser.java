package com.ubihinon.slideshow.helper;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

import com.ubihinon.slideshow.model.Image;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by ubihinon on 30.05.16.
 * This class get load images from a file and the internet
 */
public class ImageParser {
    private ArrayList<Image> images;


    public ImageParser(ArrayList<Image> images) {
        this.images = images;
    }

    /**
     * Parse images list from local file
     * @return JSON string
     */
    public String loadJSONFromFile(String fileName, Resources resources,
                                   String packageName) {

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        InputStream inputStream = resources.openRawResource(
                resources.getIdentifier(fileName, "raw", packageName)
        );
        byte[] buffer = new byte[1024];
        int len;
        try {
            while((len = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, len);
            }
            outputStream.close();
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outputStream.toString();
    }

    /**
     * Parse json string to image
     * @param data The JSON string
     * @return {@link ArrayList<Image>}
     */
    public ArrayList<Image> parseImages(String data) {
        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONArray jsonArray = jsonObject.getJSONArray("images");

            for(int i = 0; i < jsonArray.length(); i++) {
                Image image = new Image();
                image.setId(
                        (int) jsonArray.getJSONObject(i).get("id")
                );
                image.setUrl(
                        jsonArray.getJSONObject(i).get("url").toString()
                );
                images.add(image);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return images;
    }
    /**
     * Load image from the internet by {@link Image#getUrl()}
     * and save it to {@link ArrayList<Image>}
     */
    public void setDrawablesFromUrl(int i) throws Exception {
        Image image = images.get(i);
        InputStream stream = (InputStream) new URL(image.getUrl()).getContent();
        Drawable drawable = Drawable.createFromStream(stream, "image.jpg");
        if(drawable == null) {
            throw new FileNotFoundException("drawable is null");
        }
        image.setDrawable(drawable);
        stream.close();
    }

    public ArrayList<Image> getImages() {
        return images;
    }
}
